package slick.jdbc

import java.sql.{Blob, CallableStatement, Clob, Connection, DatabaseMetaData, NClob, PreparedStatement, SQLClientInfoException, SQLException, SQLWarning, SQLXML, Savepoint, Statement, Struct}
import java.util.Properties
import java.util.concurrent.Executor

import org.slf4j.LoggerFactory

class ConnectionWrapper(con: Connection) extends Connection {

  private val logger = LoggerFactory.getLogger(classOf[ConnectionWrapper])

  /**
    * Close the underlying connection.
    *
    * @throws SQLException
    * the exception
    */
  @throws[SQLException]
  def closeRealConnection() {
    logger.info("Closing REAL connection {}", hashCode())
    con.close()
  }

  @throws[SQLException]
  def commitReal() {
    logger.info("Committing FOR REAL on connection {}", hashCode())
    con.commit()
  }

  @throws[SQLException]
  def setAutoCommitReal(autoCommit: Boolean) {
    logger.info("setAutoCommit to {} FOR REAL on connection {}", autoCommit, hashCode())
    con.setAutoCommit(autoCommit)
  }

  @throws[SQLException]
  override def close() {
    logger.debug("NOT closing connection {}", hashCode())
  }

  @throws[SQLException]
  override def commit() {
    logger.debug(" NOT Committing connection {}", hashCode())
  }

  @throws[SQLException]
  override def setAutoCommit(autoCommit: Boolean) {
    logger.debug("NOT setting autoCommit to {} on connection {}", autoCommit, hashCode())
  }

  @throws[SQLException]
  override def getAutoCommit: Boolean = {
    logger.info("getAutoCommit for connection {}", hashCode())
    con.getAutoCommit
  }

  @throws[SQLException]
  override def clearWarnings() {
    con.clearWarnings()
  }

  @throws[SQLException]
  override def createStatement: Statement = con.createStatement

  @throws[SQLException]
  override def createStatement(resultSetType: Int, resultSetConcurrency: Int): Statement = con.createStatement(resultSetType, resultSetConcurrency)

  @throws[SQLException]
  override def createStatement(resultSetType: Int, resultSetConcurrency: Int, resultSetHoldability: Int): Statement = con.createStatement(resultSetType, resultSetConcurrency, resultSetHoldability)

  @throws[SQLException]
  override def getCatalog: String = con.getCatalog

  @throws[SQLException]
  override def getHoldability: Int = con.getHoldability

  @throws[SQLException]
  override def getMetaData: DatabaseMetaData = con.getMetaData

  @throws[SQLException]
  override def getTransactionIsolation: Int = con.getTransactionIsolation

  @throws[SQLException]
  override def getTypeMap: java.util.Map[String, Class[_]] = con.getTypeMap

  @throws[SQLException]
  override def getWarnings: SQLWarning = con.getWarnings

  @throws[SQLException]
  override def isClosed: Boolean = con.isClosed

  @throws[SQLException]
  override def isReadOnly: Boolean = con.isReadOnly

  @throws[SQLException]
  override def nativeSQL(sql: String): String = con.nativeSQL(sql)

  @throws[SQLException]
  override def prepareCall(sql: String): CallableStatement = con.prepareCall(sql)

  @throws[SQLException]
  override def prepareCall(sql: String, resultSetType: Int, resultSetConcurrency: Int): CallableStatement = con.prepareCall(sql, resultSetType, resultSetConcurrency)

  @throws[SQLException]
  override def prepareCall(sql: String, resultSetType: Int, resultSetConcurrency: Int, resultSetHoldability: Int): CallableStatement = con.prepareCall(sql, resultSetType, resultSetConcurrency, resultSetHoldability)

  @throws[SQLException]
  override def prepareStatement(sql: String): PreparedStatement = con.prepareStatement(sql)

  @throws[SQLException]
  override def prepareStatement(sql: String, autoGeneratedKeys: Int): PreparedStatement = con.prepareStatement(sql, autoGeneratedKeys)

  @throws[SQLException]
  override def prepareStatement(sql: String, columnIndexes: Array[Int]): PreparedStatement = con.prepareStatement(sql, columnIndexes)

  @throws[SQLException]
  override def prepareStatement(sql: String, columnNames: Array[String]): PreparedStatement = con.prepareStatement(sql, columnNames)

  @throws[SQLException]
  override def prepareStatement(sql: String, resultSetType: Int, resultSetConcurrency: Int): PreparedStatement = con.prepareStatement(sql, resultSetType, resultSetConcurrency)

  @throws[SQLException]
  override def prepareStatement(sql: String, resultSetType: Int, resultSetConcurrency: Int, resultSetHoldability: Int): PreparedStatement = con.prepareStatement(sql, resultSetType, resultSetConcurrency, resultSetHoldability)

  @throws[SQLException]
  override def releaseSavepoint(savepoint: Savepoint) {
    con.releaseSavepoint(savepoint)
  }

  @throws[SQLException]
  override def rollback() {
    logger.info("Rollback connection {}", hashCode())
    con.rollback()
  }

  @throws[SQLException]
  override def rollback(savepoint: Savepoint) {
    logger.info("Rollback savepoint for connection {}", hashCode())
    con.rollback(savepoint)
  }

  @throws[SQLException]
  override def setCatalog(catalog: String) {
    con.setCatalog(catalog)
  }

  @throws[SQLException]
  override def setHoldability(holdability: Int) {
    con.setHoldability(holdability)
  }

  @throws[SQLException]
  override def setReadOnly(readOnly: Boolean) {
    logger.info("setReadOnly on connection {}", hashCode())
    con.setReadOnly(readOnly)
  }

  @throws[SQLException]
  override def setSavepoint(): Savepoint = {
    logger.info("setSavepoint on connection {}", hashCode())
    con.setSavepoint()
  }

  @throws[SQLException]
  override def setSavepoint(name: String): Savepoint = {
    logger.info("setSavepoint on connection {}", hashCode())
    con.setSavepoint(name)
  }

  @throws[SQLException]
  override def setTransactionIsolation(level: Int) {
    logger.info("setTransactionIsolation to {} on connection {}", level, hashCode())
    con.setTransactionIsolation(level)
  }

  override def toString: String = if (con != null) con.toString else ""

  @throws[SQLException]
  override def createArrayOf(typeName: String, elements: Array[AnyRef]): java.sql.Array = con.createArrayOf(typeName, elements)

  @throws[SQLException]
  override def createBlob: Blob = con.createBlob

  @throws[SQLException]
  override def createClob: Clob = con.createClob

  @throws[SQLException]
  override def createNClob: NClob = con.createNClob

  @throws[SQLException]
  override def createSQLXML: SQLXML = con.createSQLXML

  @throws[SQLException]
  override def createStruct(typeName: String, attributes: Array[AnyRef]): Struct = createStruct(typeName, attributes)

  @throws[SQLException]
  override def setSchema(schema: String) {
    con.setSchema(schema)
  }

  @throws[SQLException]
  override def getSchema: String = con.getSchema

  @throws[SQLException]
  override def abort(executor: Executor) {
    con.abort(executor)
  }

  @throws[SQLException]
  override def setNetworkTimeout(executor: Executor, milliseconds: Int) {
    con.setNetworkTimeout(executor, milliseconds)
  }

  @throws[SQLException]
  override def getNetworkTimeout: Int = con.getNetworkTimeout

  @throws[SQLException]
  override def getClientInfo: Properties = con.getClientInfo

  @throws[SQLException]
  override def getClientInfo(name: String): String = con.getClientInfo(name)

  @throws[SQLException]
  override def isValid(timeout: Int): Boolean = con.isValid(timeout)

  @throws[SQLClientInfoException]
  override def setClientInfo(properties: Properties) {
    con.setClientInfo(properties)
  }

  @throws[SQLClientInfoException]
  override def setClientInfo(name: String, value: String) {
    con.setClientInfo(name, value)
  }

  @throws[SQLException]
  override def setTypeMap(map: java.util.Map[String, Class[_]]) {
    con.setTypeMap(map)
  }

  @throws[SQLException]
  override def isWrapperFor(iface: Class[_]): Boolean = con.isWrapperFor(iface)

  @throws[SQLException]
  override def unwrap[T](iface: Class[T]): T = {
    con.unwrap(iface)
  }
}
