package slick.jdbc

import java.sql.Connection

class HodDataSource extends DatabaseUrlDataSource {

  @volatile private[this] var initialized = false

  override def init: Unit = if(!initialized) {
    setUrl("jdbc:log4jdbc:mysql://localhost:3306/activiti")
    setUser("activiti_user")
    setPassword("Phantaloon1!")
    setDriverClassName("net.sf.log4jdbc.DriverSpy")
    initialized = true
    super.init
  }

  override def getConnection: Connection = {
    var connection = HodDataSource.getCurrentConnection
    if (connection == null) {
      connection = new ConnectionWrapper(super.getConnection)
      HodDataSource.setCurrentConnection(connection)
    }

    connection
  }

  //TODO: BT: need to override the overloaded getConnection method as well
}

object HodDataSource {

  private val currentConnection = new ThreadLocal[Connection]

  def setCurrentConnection(connection: Connection): Unit = {
    currentConnection.set(connection)
  }

  def getCurrentConnection: Connection = currentConnection.get

  def removeCurrentConnection(): Unit = {
    currentConnection.remove()
  }
}
